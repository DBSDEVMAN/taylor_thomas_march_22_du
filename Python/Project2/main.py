from album import Album #abstraction provides standard interface for multiple components.
from artist import Artist
from song import Song
from datetime import timedelta, date
import pandas as pd
import csv


if __name__ == "__main__": 

    albums_csv = 'data/albums.csv'
    artists_csv = 'data/artists.csv'
    songs_csv = 'data/songs.csv'
    albumsxsongs_csv = 'data/albums_x_songs.csv'
    artistsxalbums_csv = 'data/artists_x_albums.csv'

    albums_df = pd.read_csv(albums_csv)
    artists_df = pd.read_csv(artists_csv)
    songs_df = pd.read_csv(songs_csv)
    albumsxsongs_df = pd.read_csv(albumsxsongs_csv)
    artistsxalbums_df = pd.read_csv(artistsxalbums_csv)

    artists_dict = artists_df.to_dict()
    albums_dict = albums_df.to_dict()
    songs_dict = songs_df.to_dict()

    #songs_dict = songs_df.to_dict(orient='records')


    #song1 = song(songs_dict['name'][0], songs_dict['release_date'][0])
    song1 = Song(songs_dict['name'][0], songs_dict['duration'][0], songs_dict['release_date'][0])
    album1 = Album(albums_dict['title'][0], albums_dict['release_date'][0])
    #album1 = Album(albums_df['title'], albums_df['release_date'])
    print(song1)
    print(album1)
    



    #print(albums_df.head(1))
    #print(artists_df.head(1))
    #print(songs_df.head(1))
    

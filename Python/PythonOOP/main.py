from asyncio.tasks import _T4
from album import Album #abstraction provides standard interface for multiple components.
from artist import Artist
from song import Song
from datetime import timedelta, date


#main program
#creates a bunch of instances
if __name__ == "__main__":
    #create an artist
    nF = Artist('NF', ['Rap'], ['Nate'])

    #create and album
    t1 = Song('CLOUDS', timedelta(minutes=4, seconds=4), date(2021, 3, 26))
    t2 = Song('THATS A JOKE', timedelta(minutes=3, seconds=30), date(2021, 3, 26))
    t3 = Song('JUST LIKE YOU', timedelta(minutes=4, seconds=4), date(2021, 3, 26))
    t4 = Song('CLOUDS', timedelta(minutes=4, seconds=4), date(2021, 3, 26))
    t5 = Song('CLOUDS', timedelta(minutes=4, seconds=4), date(2021, 3, 26))
    t6 = Song('LOST', timedelta(minutes=3, seconds=55), date(2021, 3, 26))
    t7 = Song('PAID MY DUES', timedelta(minutes=3, seconds=32), date(2021, 3, 26))
    #album declaration
    cloud = Album('CLOUDS (The Mixtape)', date(2021, 3, 26))
    #album title
    at = cloud.title
    nF.albums.append(cloud)
    for song in nF.albums:
        if song.title == at:
            song.song_list += [t1, t2, t3]
    
    print(nF.count_songs())
    album_duration = cloud.total_duration()
    print(album_duration)
    print('Album', cloud, 'by the artist', nF.name, 'with a duration of', album_duration)
    
from datetime import timedelta, date

class Album():

    def __init__(self, title, date, song_list=None):

        self.title = title

        self.date = date

        if song_list is None:

            self.song_list = []

        else:

            self.song_list = song_list

    #debugging        

    def __repr__(self):

        return self.title

   

    def count_songs(self):

        return len(self.song_list)

   

    def total_duration(self):

        time = sum([song_list.duration for song_list in self.song_list], timedelta())

        return time
package Pokemon;

import java.util.ArrayList;
import java.util.Random;

public class Pokemon{
    private int health;
    private int strength;
    private int speed;
    private String name;
    private int maxHealth;
    private String type;

    /**
     * Constructs the pokemon
     * 
     * @Require:
     *    health is an integer greater than or equal to 1 but less than or equal to 300
     *    strength is and integer greater than or equal to 1 but less than or equal to 300
     *    speed is an integer greater than or equal to 1 but less than or equal to 300
     * 
     * 
     *  
     */
    public Pokemon(int health, int strength, int speed, String name){
        if (inRange(health)){
            this.health = health;
            this.maxHealth = health;
        }
        else{
            throw new IllegalArgumentException("Invalid Input Range, Health must be between 1 and 300 points.");
        }
        if (inRange(strength)){
            this.strength = strength;
        }
        else{
            throw new IllegalArgumentException("Invalid Input Range, Health must be between 1 and 300 points.");
        }
        if (inRange(speed)){
            this.speed = speed;
        }
        else{
            throw new IllegalArgumentException("Invalid Input Range, Health must be between 1 and 300 points.");
        }
        this.name = name;

    }

    public boolean inRange(int num){
        return num >= 1 && num <= 300;
    }

    public void pokemonCenter(){
        this.health = this.maxHealth;
    }

    // Write a program to handle a battle between two pokemon
    // We want this to show what the health is through each round of a battle
    // Be creative with this
    // HINT : You will need to make sure you battle until 1 pokemon has 0 health (Think of possible loops to use)
    public static void battle(Pokemon pokemon1, Pokemon pokemon2){
        Pokemon attacker1;
        //Pokemon attacker2;

        //determine who will go first based on speed
        if (pokemon1.speed > pokemon2.speed){
            attacker1 = pokemon1;
            //attacker2 = pokemon2;
        } else if (pokemon2.speed > pokemon1.speed){
            attacker1 = pokemon2;
            //attacker2 = pokemon1;
        } else {
            Pokemon[] randomPick = {pokemon1, pokemon2};
            int randomNumber = new Random().nextInt(randomPick.length);
            attacker1 = randomPick[randomNumber];
            //attacker2 = randomPick[1-randomNumber];
        }

        Pokemon currentAttacker = attacker1;
        Pokemon currentOpponent;
        int round = 1;
       
        //runs on while loop until 1 pokemon has 0 health and other has 1
        while(pokemon1.health > 0 && pokemon2.health > 0){
            currentOpponent = (pokemon1.name == currentAttacker.name) ? pokemon2 : pokemon1; 
            System.out.println("Round " + round + ": " + currentAttacker.name + " vs " + currentOpponent.name);
            round++;
        //attack ensues, update health based on damage
            int variation = 90 + new Random().nextInt(20);
            int attackDamage = (currentAttacker.strength / 2) * variation / 100;

            currentOpponent.health -= attackDamage; 

            System.out.println(currentAttacker.name + " attacks " + currentOpponent.name + ". Damage to " + currentAttacker.name + " is " + attackDamage);

            if (currentOpponent.health < 0){
                currentOpponent.health = 0;
                System.out.println("");
                System.out.println(currentOpponent.name + " has fainted.");
            }
            System.out.println(currentOpponent.name + " has " + currentOpponent.health + " health remaining.");
            currentAttacker = (pokemon1.name != currentAttacker.name) ? pokemon1 : pokemon2; 
            System.out.println(""); 
        }

        Pokemon winner = (pokemon1.health <= 0) ? pokemon2 : pokemon1;
        System.out.println( winner.name + " is the winner!");
        pokemon1.pokemonCenter();
        pokemon2.pokemonCenter();
        System.out.println("\n Both Pokemon were taken to the PokeCenter.");
        //each round print out health of each

        //print winner at end


    }

}

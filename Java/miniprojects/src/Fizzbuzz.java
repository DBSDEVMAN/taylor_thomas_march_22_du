public class Fizzbuzz {

    // Write a program that prints the numbers from 1 to 100
    // and for multiples of ‘3’ print “Fizz” instead of the number
    // and for the multiples of ‘5’ print “Buzz”
    // and for multiples of '15' print "FizzBuzz"

    public static void main(String args[]) {
        // Enter code here
        for (int i = 1;i <= 100;i++)
        {
            if (i%15 == 0)
            {
                System.out.println("Fizzbuzz");
            }
            else if (i%5 == 0)
            {
                System.out.println("Buzz");
            }
            else if (i%3 == 0)
            {
                System.out.println("Fizz");
            }
            else {
                System.out.println(i);
            }
        }
    }
}

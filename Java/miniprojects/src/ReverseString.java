public class ReverseString {

    // Write a program to reverse a String
    // Think outside the box and solve it multiple ways!
    public static void main(String[] args){
        System.out.println("Reverse String Methods");
        String input = "Can this be reversed";
        reverseMethod1(input);
        iterativeReverse(input);
        printReverse(input);

    }

    // Solution 1
    public static void reverseMethod1(String input){
        StringBuilder builder = new StringBuilder(input);

        System.out.println(builder.reverse());


    }

    // Solution 2
    public static void iterativeReverse(String input){
        for(int i = input.length() - 1; i >= 0; i--) {
            System.out.print(input.charAt(i));
        }
    }

    // Solution 3
    public static void printReverse(String input){
        char[] temp = input.toCharArray();
        int j = temp.length - 1;

        for(int i = 0; i < temp.length; i++, j--){
            char tmpChar = temp[i];
            temp[i] = temp[j];
            temp[j] = tmpChar;
        }

        for (char c : temp){
            System.out.println(c);
        }

        // String temp = "";
        
        // for(int i =input.length() - 1; i >= 0; i--){
        //     temp = temp + input.charAt(i); 
        // }

        // System.out.println(temp);
    }


}

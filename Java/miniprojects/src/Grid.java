import java.util.ArrayList;
import java.util.Vector;

public class Grid {

    // Write 2 different methods
    // first: prints a grid of '-' 5x10
    // second: creates an object array and then prints that object array as a grid 10x10

    public static void main(String[] args){
        printGrid();
        ArrayList<ArrayList<String>> arrList = createGridArray();
        printVector(arrList);
    }

    // Print a 5x10 grid to the terminal
    private static void printGrid(){
        // enter code here
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 5; j++){
                System.out.print("-");
            }
            System.out.println("");
        }
    }

    // write a method to make the below container into a grid
    private static ArrayList<ArrayList<String>> createGridArray(){
        // enter code here
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();

        for (int i = 0; i < 10; i++){
            ArrayList<String> row = new ArrayList<String>();

            for (int j = 0; j < 10; j++){
                row.add("-");
            }

            result.add(row);
        }
        //Way to find a exact value on the grid
        //result.get(0).get(0);
        return result;
    }

    // Write a method to print the vector in a 10x10 shape
    private static void printVector(ArrayList<ArrayList<String>> arrList){
        // enter code here
        for (ArrayList<String> row : arrList) {
            System.out.println(row);
        }
    }
}

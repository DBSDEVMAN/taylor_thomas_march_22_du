import java.util.Scanner;

public class PrimeNumbers {

    // Write a program to tell whether a number is a prime number or not
    // print if the number typed in is a Prime or Not Prime
    // Make the program continuously run until the number 0 is typed in
    //      Challenge: Think of another way to solve this problem other than your first solution
    //      Challenge 2: enhance either one of the solutions above to either simplify or speed it up
    public static void main(String args[]) {
        System.out.println("WRITE HERE");
        Scanner scnr = new Scanner(System.in);
    }


    /*
     * Java method to check if an integer number is prime or not.
     * @return true if number is prime, else false
     */
    public static boolean isPrime(int number) {
        return true;
    }


    /*
     * Second version of isPrimeNumber method, with improvements
     * @return true if number is prime, else false
     */
    public static boolean isPrimeNumber(int number) {
        return true;

    }


    /*
     * Third way to check if a number is prime or not.
     */
    public static String isPrimeOrNot(int num) {
        return "";
    }
}

package Cards;
public class Card {
    private final String suit;
    private final Rank rank;
    private final int value;
    // create the card class and constructors

    // enter code here
    public Card(String suit, Rank rank, int value){
        this.suit = suit;
        this.rank = rank;
        this.value = value;
    }


    public Rank getRank(){
        return this.rank;
    }
    public String getSuit(){
        return this.suit;
    }
    // public String printCard(){
    //     // System.out.println(this.suit + "," + this.rank + "," + this.value);


    //     return String.format("Suit is %s", this.suit);
    // }


}


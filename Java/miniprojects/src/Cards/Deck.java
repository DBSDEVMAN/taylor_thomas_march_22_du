package Cards;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Deck {
    ArrayList<Card> deck = new ArrayList<>();


    // Write a method that creates a deck of Cards (hint use Card Object)
    public Deck(){
        // enter code here
        Rank[] ranks = Rank.values();

        for(int i = 0; i < ranks.length; i++){

            Card newCard = new Card("Hearts", ranks[i], i + 1);
            deck.add(newCard);    
        }
        for(int i = 0; i < ranks.length; i++){

            Card newCard = new Card("Spades", ranks[i], i + 1);
            deck.add(newCard);    
        }
        for(int i = 0; i < ranks.length; i++){

            Card newCard = new Card("Diamonds", ranks[i], i + 1);
            deck.add(newCard);    
        }
        for(int i = 0; i < ranks.length; i++){

            Card newCard = new Card("Clubs", ranks[i], i + 1);
            deck.add(newCard);    
        }
        

    }

    // Write a method to shuffle the deck
    public void shuffleDeck(){
        // enter code here
        Collections.shuffle(deck);
    }

    // Write a method to print the deck
    public void printDeck(){
        // enter code here
        for(Card card : deck){
            System.out.println(card.getSuit() +","+ card.getRank());
        }
    }

}


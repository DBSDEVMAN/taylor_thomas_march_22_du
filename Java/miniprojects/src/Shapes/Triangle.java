package Shapes;

public class Triangle implements Shape{
    
    private double height;
    private double base;
    
    public Triangle(double height, double base){
        this.height = height;
        this.base = base;
    }
    @Override
    public double area() {
        return (this.height * this.base * 0.5);
    }

    @Override
    public int sides() {
        return 3;
    }
    
}

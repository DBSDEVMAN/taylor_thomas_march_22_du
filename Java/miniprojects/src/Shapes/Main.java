package Shapes;

public class Main {

    // Write a program that prints out the areas of a square and rectangle
    // Use the Shape Inheritance
    // For help look at the circle example
    // Print out the square and rectangle side and areas
    // Challenge: Add another shape of your choosing and add that object (make it a fun one!)
    public static void main(String[] args){
        Circle circle = new Circle(11);
        System.out.println("Circle sides: " + circle.sides() + " - Circle area: " + circle.area());

        Rectangle rectangle = new Rectangle(5,9);
        System.out.println("Rectangle sides: " + rectangle.sides() + " - Rectangle area: " + rectangle.area());

        Square square = new Square(5);
        System.out.println("Square sides: " + square.sides() + " - Square area: " + square.area());

        Triangle triangle = new Triangle(5,4);
        System.out.println("Triangle sides: " + triangle.sides() + " - Triangle area: " + triangle.area());
    }

    
}

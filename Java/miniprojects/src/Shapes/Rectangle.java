package Shapes;

public class Rectangle implements Shape{

    private double width;
    private double height;
    public Rectangle(double width, double height){
        this.height = height;
        this.width = width;
    }

    @Override
    public double area() {
        return (this.width * this.height);
    }

    @Override
    public int sides() {
        return 4;
    }
}

package Extra;

import java.util.Arrays;

/**
 * Java program - String Anagram Example.
 * This program checks if two Strings are anagrams or not
 *
 * @author Javin Paul
 */
public class AnagramCheck {

    public static void main(String args[]) {

        //finding factorial of a number in Java using recursion - Example
        if(isAnagram("test", "estt")){
            System.out.println("YAY");
        }else{
            System.out.println("NO");
        }

        if(checkAnagram("test", "estt")){
            System.out.println("YAY");
        }else{
            System.out.println("NO");
        }
    }

    /*
     * @return true, if both String are anagram
     */
    public static boolean isAnagram(String word, String anagram){
        return true;
    }

    /*
     * @return true, if both Strings are anagram.
     */
    public static boolean iAnagram(String word, String anagram){
        return true;
    }


    public static boolean checkAnagram(String first, String second){
        return true;
    }
}
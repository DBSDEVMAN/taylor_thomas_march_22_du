package Extra;

public class SymbolCount {

    // write a function that counts each symbol (letter, number, symbol) and the amount each symbol appears in the the String
    // Print out the top 10 most used symbols and the top 5 least used symbols and their counts

    // Challenge keep track of each word and the amount of times its displayed.
    // Print out the most used and least used words (there may be ties).

    String sample = "Quotations in a history essay are used in moderation and to address particulars of a " +
            "given historical event. Students who tend to use too many quotes normally lose marks for doing so. " +
            "The author of a history essay normally will read the text from a selected source, understand it, " +
            "close the source (book for web site for example?) and then condense it using their own words. " +
            "Simply paraphrasing someone else’s work is still considered to be plagiarism. " +
            "History essays may contain many short quotes.\n" +
            "Quotations of three or fewer lines are placed between double quotation marks. " +
            "For longer quotes, the left and right margins are indented by an additional 0.5” or 1 cm, the text is " +
            "single-spaced and no quotation marks are used. Footnotes are used to cite the source.\n" +
            "Single quotation marks are used for quotations within a quotation. Three ellipsis points (...) " +
            "are used when leaving part of the quotation out. Ellipsis cannot be used at the start of a quotation!!!";
}

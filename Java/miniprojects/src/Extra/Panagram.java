package Extra;

import java.util.function.IntSupplier;

public class Panagram {
//    Given an integer N, the task is to check whether the given number is a pangram or not.
//    Note: A Pangram Number contains every digit [0- 9] at least once.
    // HINT : Make sure you write in a main method (all java programs start with a certain method)

    public static void main(String[] args){
        System.out.println(isPanagram(1234567890));
        System.out.println(isPanagram(1623457830));
    }

    public static boolean isPanagram(int num){
        int[] check = new int[10];

        String string = String.valueOf(num);

        for(int i = 0; i < string.length(); i++){
            char character = string.charAt(i);
            int ascii = (int) character;
            int value = ascii - (int) '0';
            //Integer currentNum = Integer.parseInt(String.valueOf(string.charAt(i)));
            check[value] = 1;
        }
        for(Integer value : check){
            if (value == 0){
                return false;
            }
        }
        return true;
    }
}

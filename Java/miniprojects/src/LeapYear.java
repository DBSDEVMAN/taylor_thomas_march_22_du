import java.time.LocalDate;
import java.util.Calendar;

public class LeapYear {
    // Write a program to figure out what years are leap years over the next 55 years.
    // start with the current year and print every year that is a leap year
    // HINT: You may need to use some type of Java Date Utilities

    public static void main(String[] args){
        // enter code here
        int cyear = LocalDate.now().getYear();
        for (int i = cyear; i<=cyear+55; i++){
            if (isLeapYear(i)){
                System.out.println(i);
            }           
        }
        
    }

    // Use this to determine if the year is a leap year
    public static boolean isLeapYear(int year) {
        // enter code here
        if (year % 4 == 0){
            return true;
        }
        return false;
    }
}

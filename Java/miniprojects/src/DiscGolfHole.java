public class DiscGolfHole {
    //private variables declared
        //these can only be accessed by
        //public methods of class
    private int Distance;
    private String DiscName;
    private int HoleNumber;

    //get method for holeNumber to access
    //private variable HoleNumber
    public int getHoleNumber() {
        return HoleNumber;
    }

    //get method for discName to access
    //private variable DiscName
    public String getDiscName(){
        return DiscName;
    }

    //get method for distance to access
    //private variable Distance
    public int getDistance(){
        return Distance;
    }

    //set method for holeNumber to access
    //private variable HoleNumber
    public void setHoleNumber(int newHoleNumber) {
        HoleNumber = newHoleNumber;
    }

    //set method for discName to access
    //private variable DiscName
    public void setDiscName(String newDiscName) {
        DiscName = newDiscName;
    }

    //set method for distance to access
    //private variable Distance
    public void setDistance(int newDistance) {
        Distance = newDistance;
    }

    public static void main (String[] args){
        DiscGolfHole obj = new DiscGolfHole();
        // setting values of the variables
        obj.setHoleNumber(1);
        obj.setDistance(300);
        obj.setDiscName("MD3");

        //Displaying values of the variables
        System.out.println("Hole Number:" + obj.getDiscName());
        System.out.println("Distance in feet:" + obj.getDistance());
        System.out.println("Disc being thrown:" + obj.getDiscName());

    }
}

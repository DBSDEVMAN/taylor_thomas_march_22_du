package com.test.controller;

import com.test.entity.Gym;
import com.test.entity.Pokemon;
import com.test.entity.Roster;
import com.test.services.PokemonService;
import com.test.services.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


// *********** GO TO SETTINGS, Plugins and install LOMBOK **************
// pokemon class (instructor copy)
// LAB:
//      Go through and create 5 methods
//      using each of the Rest Calls we just learned (Get, post, put, delete)
// GIVEN:
//      You are given the entity and the repository class already setup for you
// GOAL:
//      By the end of this you should be able to get, add, update, and delete pokemons from the database!


@RestController
@RequestMapping(path = "/pokemon")
public class PokemonController {

    @Autowired
    PokemonService pokemonService;

    @GetMapping("")
    public ResponseEntity<Iterable<Pokemon>> getAllpokemon() {
        return new ResponseEntity<Iterable<Pokemon>>(pokemonService.getAllPokemon(), HttpStatus.OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<Pokemon> getPokemonById(@PathVariable Integer id) {
        return new ResponseEntity<Pokemon>(pokemonService.getPokemonById(id), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Pokemon> createNewPokemon(@RequestBody Pokemon pokemon) {
        Pokemon newPokemon = pokemonService.createNewPokemon(pokemon);
        return new ResponseEntity<Pokemon>(newPokemon, HttpStatus.OK);

    }

    @PutMapping("/{id}")
    public ResponseEntity<Pokemon> updatePokemon(@RequestBody Pokemon pokemon, @PathVariable Integer id) throws Exception {
        Pokemon newPokemon = pokemonService.updatePokemon(pokemon);
        return new ResponseEntity<Pokemon>(newPokemon, HttpStatus.OK);
    }

    @PutMapping("/delete/{id}")
    public ResponseEntity<Pokemon> deletePokemon(@RequestBody Pokemon pokemon, @PathVariable Integer id) throws Exception {
        pokemonService.deletePokemon(pokemon);
        return new ResponseEntity<Pokemon>(HttpStatus.OK);
    }

    @PostMapping("/battle")
    public Pokemon battle(@RequestBody Roster roster) throws Exception{
        Gym gym = new Gym();
        return gym.battle(pokemonService.getPokemonById(roster.getPokemon1()), pokemonService.getPokemonById(roster.getPokemon2()));
    }



}

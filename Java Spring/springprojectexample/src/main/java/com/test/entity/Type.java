package com.test.entity;

public enum Type {
    FIRE,
    WATER,
    GRASS
}

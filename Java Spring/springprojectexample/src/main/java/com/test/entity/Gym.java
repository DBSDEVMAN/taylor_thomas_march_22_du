package com.test.entity;

import java.util.Random;

public class Gym {


//
//    public void pokemonCenter(){
//        this.health = this.maxHealth;

    public Pokemon battle(Pokemon pokemon1, Pokemon pokemon2) {
        Pokemon attacker1;
        //Pokemon attacker2;

        //determine who will go first based on speed
        if (pokemon1.getSpeed() > pokemon2.getSpeed()) {
            attacker1 = pokemon1;
            //attacker2 = pokemon2;
        } else if (pokemon2.getSpeed() > pokemon1.getSpeed()) {
            attacker1 = pokemon2;
            //attacker2 = pokemon1;
        } else {
            Pokemon[] randomPick = {pokemon1, pokemon2};
            int randomNumber = new Random().nextInt(randomPick.length);
            attacker1 = randomPick[randomNumber];
            //attacker2 = randomPick[1-randomNumber];
        }

        Pokemon currentAttacker = attacker1;
        Pokemon currentOpponent;
        int round = 1;

        //runs on while loop until 1 pokemon has 0 health and other has 1
        while (pokemon1.getHealth() > 0 && pokemon2.getHealth() > 0) {
            currentOpponent = (pokemon1.getName() == currentAttacker.getName()) ? pokemon2 : pokemon1;
            System.out.println("Round " + round + ": " + currentAttacker.getName() + " vs " + currentOpponent.getName());
            round++;
            //attack ensues, update health based on damage
            int variation = 90 + new Random().nextInt(20);
            int attackDamage = (currentAttacker.getAttack() / 2) * variation / 100;

            currentOpponent.setHealth(currentOpponent.getHealth() - attackDamage);

            System.out.println(currentAttacker.getName() + " attacks " + currentOpponent.getName() + ". Damage to " + currentAttacker.getName() + " is " + attackDamage);

            if (currentOpponent.getHealth() < 0) {
                currentOpponent.setHealth(0);
                System.out.println("");
                System.out.println(currentOpponent.getName() + " has fainted.");
            }
            System.out.println(currentOpponent.getName() + " has " + currentOpponent.getHealth() + " health remaining.");
            currentAttacker = (pokemon1.getName() != currentAttacker.getName()) ? pokemon1 : pokemon2;
            System.out.println("");
        }

        Pokemon winner = (pokemon1.getHealth() <= 0) ? pokemon2 : pokemon1;
        Pokemon loser = (winner.getName() == pokemon1.getName()) ? pokemon2 : pokemon1;
        winner.setAttack(winner.getAttack() + 2);
        winner.setHealth(winner.getHealth() + 1);

        if (winner.getLevel() < loser.getLevel()){
            winner.setLevel(winner.getLevel() + 1);
        }

        loser.setAttack(loser.getAttack() - 1);
        loser.setHealth(loser.getHealth() - 1);

        winner.setHealth((winner.getMaxHealth()));
        loser.setHealth(winner.getMaxHealth());

        winner.setTotalWins(winner.getTotalWins() + 1);
        loser.setTotalLosses(loser.getTotalLosses() + 1);

        System.out.println(winner.getName() + " is the winner!");
        //pokemon1.pokemonCenter();
        //pokemon2.pokemonCenter();
        System.out.println("\n Both Pokemon were taken to the PokeCenter.");
        return winner;
    }

}
package com.test.entity;

public class Roster {
    private int pokemon1;
    private int pokemon2;

    public int getPokemon1(){
        return this.pokemon1;
    }

    public int getPokemon2(){
        return this.pokemon2;
    }
}

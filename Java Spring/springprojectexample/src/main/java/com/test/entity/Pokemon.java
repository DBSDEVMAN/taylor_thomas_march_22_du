package com.test.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="pokemon")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Pokemon {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name="name")
    String name;

    @Column(name = "health")
    Integer health;

    @Column(name="attack")
    Integer attack;

    @Column(name="speed")
    Integer speed;

    @Column(name="maxHealth")
    Integer maxHealth;

    @Column(name="type")
    Type type;

    @Column(name="description")
    String description;

    @Column(name="totalWins")
    String totalWins;

    @Column(name="totalLosses")
    String totalLosses;

    @Column(name="level")
    Integer level;
}

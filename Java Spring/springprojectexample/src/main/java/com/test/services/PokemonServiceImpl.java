package com.test.services;

import com.test.entity.Pokemon;
import com.test.repository.PokemonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

// You will need to create the various methods and look into Crud Repositories (similar to JPA)
@Service
@Slf4j // this is used for logging as you see below
public class PokemonServiceImpl implements PokemonService {

    @Autowired
    PokemonRepository pokemonRepository;

    @Override
    public Iterable<Pokemon> getAllPokemon(){
        log.info("Getting all Pokemon");
        return pokemonRepository.findAll();
    }

    public Pokemon getPokemonById(Integer id){
        log.info("Getting one pokemon");
        Optional<Pokemon> pokemon = pokemonRepository.findById(id);
        return pokemon.orElse(null);
    }

    public Pokemon createNewPokemon(Pokemon pokemon) {
        log.info("Creating Pokemon");
        Pokemon newPokemon = pokemonRepository.save(pokemon);
        return newPokemon;
    }

    public Pokemon updatePokemon(Pokemon pokemon) throws Exception {
        log.info("Updating Pokemon");
        if(pokemonRepository.existsById(pokemon.getId())){
            Pokemon updatedPokemon = pokemonRepository.save(pokemon);
            return updatedPokemon;
        }
        else {
            throw new Exception("Pokemon doesn't exist");
        }
    }

    public String deletePokemon(Pokemon pokemon) throws Exception{
        log.info("Deleting Pokemon");
        if(pokemonRepository.existsById(pokemon.getId())){
            pokemonRepository.delete(pokemon);
            return "Pokemon Deleted";
        }
        else{
            throw new Exception("Pokemon doesn't exist");
        }
    }
}


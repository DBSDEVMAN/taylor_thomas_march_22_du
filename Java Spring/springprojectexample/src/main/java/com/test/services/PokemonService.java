package com.test.services;

import com.test.entity.Pokemon;

public interface PokemonService {
    public Iterable<Pokemon> getAllPokemon();

    Pokemon createNewPokemon(Pokemon pokemon);

    Pokemon updatePokemon(Pokemon pokemon) throws Exception;

    String deletePokemon(Pokemon pokemon) throws Exception;

    public Pokemon getPokemonById(Integer id);



}
